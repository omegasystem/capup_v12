# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    def _get_default_user_id(self):
        if self.env.user.partner_id and self.env.user.partner_id.parent_id:
            user_id = self.env['res.users'].search([
                ('partner_id', '=', self.env.user.partner_id.parent_id.id)
            ])
            return user_id.id

    job_position = fields.Char('Job Position')
    phone_ext = fields.Char('Phone Number Ext.')
    document_ids = fields.One2many('partner.address', 'partner_id', string='Documents')
    company_reg_type = fields.Selection([
        ('ltd', 'Ltd Company'),
        ('private', 'Private Limited'),
        ('partner', 'PartnerShip'),
        ('proprietorship', 'Proprietorship')
    ], string='Company Reg. Type')
    status = fields.Selection([
        ('active', 'Active'),
        ('deactive', 'Deactive'),
        ('pending', 'Pending')
    ], string='Vendor Status')
    approved_by = fields.Many2one('res.users', string='Approved By')
    approved_date = fields.Date('Approved Date')
    ref_partner_id = fields.Many2one('res.partner', string='Ref. Partner')
    client_id = fields.Many2one('res.partner', string='Client', default=_get_default_user_id)
    created_by = fields.Many2one('res.users', string='Created By')


class DocumentType(models.Model):
    _name = "document.type"

    name = fields.Char('Name')


class PartnerAddress(models.Model):
    _name = "partner.address"

    partner_id = fields.Many2one('res.partner', string='Partner')
    document_type = fields.Many2one('document.type', string="Document Type")
    attachments = fields.Binary('Attachment')


class ResPartnerBank(models.Model):
    _inherit = "res.partner.bank"

    acc_type = fields.Selection(readonly=False, default='current')

    @api.model
    def _get_supported_account_types(self):
        return [('bank', _('Normal')), ('saving', ('Saving')), ('current', ('Current'))]



    # acc_type = fields.Selection([('saving', 'Saving'), ('current', 'Current')])
