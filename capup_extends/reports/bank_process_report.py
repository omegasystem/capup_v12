#-*- coding:utf-8 -*-
from odoo import api, models, _
from datetime import datetime


class BankProcessReport(models.AbstractModel):
    _name = 'report.capup_extends.report_bank_process_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, lines):
        wizard_record = data.get('data')
        active_id = data.get('context')['active_id']
        sheet = workbook.add_worksheet("")
        # sheet.set_column(1, 1, 20)
        sheet.set_column(14, 0, 20)
        row = 0
        column = 0
        sheet.write(row, column, "Client_Code")
        sheet.write(row, column + 1, "Product_Code")
        sheet.write(row, column + 2, "Payment_Type")
        sheet.write(row, column + 3, "Payment_Ref_No.")
        sheet.write(row, column + 4, "Payment_Date")
        sheet.write(row, column + 5, "Instrument Date")
        sheet.write(row, column + 6, "Dr_Ac_No")
        sheet.write(row, column + 7, "Amount")
        sheet.write(row, column + 8, "Bank_Code_Indicator")
        sheet.write(row, column + 9, "Beneficiary_Code")
        sheet.write(row, column + 10, "Beneficiary_Name")
        sheet.write(row, column + 11, "Beneficiary_Bank")
        sheet.write(row, column + 11, "Beneficiary_Branch / IFSC Code")
        sheet.write(row, column + 12, "Beneficiary_Acc_No")
        sheet.write(row, column + 13, "Location")
        sheet.write(row, column + 14, "Print_Location")
        sheet.write(row, column + 15, "Instrument_Number")
        sheet.write(row, column + 16, "Ben_Add1")
        sheet.write(row, column + 17, "Ben_Add2")
        sheet.write(row, column + 18, "Ben_Add3")
        sheet.write(row, column + 19, "Ben_Add4")
        sheet.write(row, column + 20, "Beneficiary_Email")
        sheet.write(row, column + 21, "Beneficiary_Mobile")
        sheet.write(row, column + 22, "Debit_Narration")
        sheet.write(row, column + 23, "Credit_Narration")
        sheet.write(row, column + 24, "Payment Details 1")
        sheet.write(row, column + 25, "Payment Details 2")
        sheet.write(row, column + 26, "Enrichment_1")
        sheet.write(row, column + 27, "Enrichment_2")
        sheet.write(row, column + 28, "Enrichment_3")
        sheet.write(row, column + 29, "Enrichment_4")

        row = 1
        column = 0
        client_settlement= self.env['client.settlement'].browse(active_id)
        bank_ids = self.env['res.partner.bank'].search([
            ('partner_id', '=', client_settlement.client_id.id)
        ], limit=1)
        sheet.write(row, column, "CAPUP")
        sheet.write(row, column + 1, "VPAY")
        sheet.write(row, column + 2, client_settlement.payment_type)
        sheet.write(row, column + 3, client_settlement.transaction_number)
        sheet.write(row, column + 4, client_settlement.payment_date)
        sheet.write(row, column + 5, "Instrument Date")
        if bank_ids:
            sheet.write(row, column + 6, bank_ids.acc_number)
        else:
            sheet.write(row, column + 6, "")
        sheet.write(row, column + 7, client_settlement.settlement_amount)
        sheet.write(row, column + 8, "Bank_Code_Indicator")
        if bank_ids:
            sheet.write(row, column + 9, bank_ids.bank_bic)
        else:
            sheet.write(row, column + 9, "")
        if bank_ids:
            sheet.write(row, column + 10, bank_ids.acc_holder_name)
        else:
            sheet.write(row, column + 10, "")
        if bank_ids:
            sheet.write(row, column + 11, bank_ids.bank_id.name)
        else:
            sheet.write(row, column + 11, "")
        if bank_ids:
            sheet.write(row, column + 12, bank_ids.bank_bic)
        else:
            sheet.write(row, column + 12, "")
        sheet.write(row, column + 12, "Beneficiary_Acc_No")
        sheet.write(row, column + 13, "Location")
        sheet.write(row, column + 14, "Print_Location")
        sheet.write(row, column + 15, "Instrument_Number")
        sheet.write(row, column + 16, "Ben_Add1")
        sheet.write(row, column + 17, "Ben_Add2")
        sheet.write(row, column + 18, "Ben_Add3")
        sheet.write(row, column + 19, "Ben_Add4")
        sheet.write(row, column + 20, "Beneficiary_Email")
        sheet.write(row, column + 21, "Beneficiary_Mobile")
        sheet.write(row, column + 22, "Debit_Narration")
        sheet.write(row, column + 23, "Credit_Narration")
        sheet.write(row, column + 24, "Payment Details 1")
        sheet.write(row, column + 25, "Payment Details 2")



