# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class CreateUser(models.TransientModel):
    _name = 'create.user'

    login = fields.Char('Email')
    password = fields.Char('Password')

    @api.multi
    def create_user(self):
        user_obj = self.env['res.users']
        exit_id = user_obj.search([
            ('login', '=', self.login)
        ])
        if exit_id:
            raise ValidationError(_(
                'This login already exist in system!'))
        user_id = user_obj.sudo().create({
            'company_id': self.env.ref("base.main_company").id,
            'name': self.login,
            'login': self.login,
            'email': self.login,
            # 'groups_id': [(6, 0, [self.ref('sales_team.group_sale_manager')])]
        })
        if self.env.user.partner_id and self.env.user.partner_id.parent_id:
            client_id = self.env.user.partner_id.parent_id.id if self.env.user.partner_id.parent_id else self.env.user.partner_id.id
            user_id.partner_id.write({'created_by': self.env.user.id, 'supplier': True, 'client_id': client_id})
            template_id = self.env.ref(
                'capup_extends.email_template_vendor_register',
                raise_if_not_found=False)
            self.env['scheduled.mail'].create({
                'client_id': user_id.partner_id.id,
                'scheduled_type': 'draft',
                'email_template_id': template_id.id,
                'schedule_date': fields.Date.today()
            })



