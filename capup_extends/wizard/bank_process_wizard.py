# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class BankProcess(models.TransientModel):
    _name = 'bank.process'

    @api.multi
    def download_file(self):
        # domain = [
        #     ('do_dispatch_date', '>=', self.start_date),
        #     ('do_dispatch_date', '<=', self.end_date)
        # ]
        # if self.user_ids:
        #     users_list = []
        #     for user_id in self.user_ids:
        #         users_list.append(user_id.id)
        #     domain += [('user_id', 'in', users_list)]
        # list_data = []
        # if self.invoice_state == 'paid' or self.invoice_state == 'paid_dispatched':
        #     domain += [('state', '=', 'paid')]
        # elif self.invoice_state == "open_paid":
        #     domain += [('state', 'in', ['open', 'paid'])]
        # else:
        #     domain += [('state', 'in', ['draft', 'open', 'paid'])]
        # orders = self.env['account.invoice'].search(domain)
        # for order in orders:
        #     list_data.append(order.id)

        data = {
            'ids': self,
            'model': 'client.settlement',
            # 'record': list_data,
            'data': self.read()[0]
        }
        return self.env.ref('capup_extends.report_bank_process_xlsx').report_action(self, data=data)
