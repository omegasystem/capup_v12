# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class FollowupWizard(models.TransientModel):
    _name = 'followup.wizard'

    next_followup_date = fields.Date('Next Followup Date')
    desc = fields.Text('Reason')
    received_amount = fields.Float('Received Amount')

    @api.multi
    def submit(self):
        active_id = self.env['crm.lead'].browse(self.env.context.get('active_ids'))
        leads = self.env['crm.lead'].search([
            ('user_id', '=', self.env.user.id),
            ('is_running', '=', True),
            ('id', '!=', active_id.id)
        ])
        if leads:
            raise UserError(_('Kindly close your other followup leads to proceed this one!'))
        folloup_id = self.env['follow.up.details'].create({
            'lead_id': active_id.id,
            'user_id': self.env.user.id,
            'start_timedate': fields.Datetime.now(),
            'desc': self.desc
        })
        active_id.write({
            'next_followup_date': self.next_followup_date,
            'is_running': True,
            'running_followup_id': folloup_id.id
        })

