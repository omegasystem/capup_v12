from odoo import api, fields, models, _
import datetime


class SettlementReportWizard(models.TransientModel):
    _name = 'settlement.report.wizard'

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('send_to_approve', 'Send To Approve'),
        ('approved', 'Approved - Settlement in-progress'),
        ('disapproved', 'Disapproved'),
        ('cancel', 'Cancelled'),
        ('on_hold', 'On Hold'),
        ('complete', 'Complete'),
        ('settled', 'Settled')
    ], string='Status', default='draft')

    @api.multi
    def print_report(self):
        domain = [
            ('settlement_date', '>=', self.start_date),
            ('settlement_date', '<=', self.end_date)
        ]
        if self.state:
            domain += [('state', '=', self.state)]
        settlement_ids = self.env['client.settlement'].search(domain)
        [data] = self.read()
        datas = {
            'ids': [],
            'model': 'client.settlement',
            'form': data,
            'settlement_ids': [i.id for i in settlement_ids]
        }
        return self.env.ref('capup_extends.action_settlement_report').report_action(settlement_ids, data=datas)


class ClientTotalSettlement(models.TransientModel):
    _name = 'client.total.settlement'

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    client_id = fields.Many2one('res.partner', string='Client')

    @api.multi
    def print_report(self):
        client_domain = []
        if self.client_id:
            client_domain += [('id', '=', self.client_id.id)]
        all_clients = self.env['res.partner'].search(client_domain)

        [data] = self.read()
        datas = {
            'ids': [],
            'model': 'client.settlement',
            'form': data,
            'all_clients': [i.id for i in all_clients]
        }
        return self.env.ref('capup_extends.action_client_total_settlement').report_action(all_clients, data=datas)


class ClientTotalPending(models.TransientModel):
    _name = 'client.total.pending'

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    client_id = fields.Many2one('res.partner', string='Client')

    @api.multi
    def print_report(self):
        client_domain = []
        if self.client_id:
            client_domain += [('id', '=', self.client_id.id)]
        all_clients = self.env['res.partner'].search(client_domain)
        [data] = self.read()
        datas = {
            'ids': [],
            'model': 'client.settlement',
            'form': data,
            'all_clients': [i.id for i in all_clients]
        }
        return self.env.ref('capup_extends.action_client_total_pending').report_action(all_clients, data=datas)
