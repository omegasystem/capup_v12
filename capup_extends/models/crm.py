# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
from odoo.exceptions import UserError, ValidationError
from datetime import date
from dateutil.relativedelta import relativedelta


class CrmLead(models.Model):
    _inherit = "crm.lead"

    settlement_id = fields.Many2one('client.settlement', string='Settlement')
    vendor_settlement_id = fields.Many2one('vendor.settlement', string='Settlement')
    invoice_id = fields.Many2one('account.invoice', string='Invoice')
    followup_ids = fields.One2many('follow.up.details', 'lead_id', string='Followups')
    is_running = fields.Boolean('Start Followup')
    running_followup_id = fields.Many2one('follow.up.details', string='Running Followup')
    followup_status = fields.Selection([
        ('under_process', 'Under Process'),
        ('normal', 'Normal')
    ], string='Followup Status')
    next_followup_date = fields.Date('Next Followup Date')
    payment_status = fields.Selection([
        ('paid', 'Paid'),
        ('partial', 'Partial')
    ], compute='_get_payment_status', string='Payment Status')
    received_amount = fields.Float(string='Received Amount')
    penalty_amount = fields.Float(string='Penalty Amount')
    penalty_days = fields.Float(compute='_get_penalty_days', string='Penalty Days')
    receivable_amount = fields.Float(compute='_get_payment_status', string='Receivable Amount')
    settlement_date = fields.Date(string='Settlement Date')
    due_date = fields.Date(string='Due Date')
    due_amount = fields.Float(string='Due Amount')
    date_of_receipt = fields.Date('Date of Receipt')
    discounted_amount = fields.Float('Discounted Amount')
    waved_off_amount = fields.Float('Waved off Amount')
    receive_in_excess = fields.Float('Received in Excess')
    extra_amount = fields.Float('Extra Amount')
    tentative_date_payment = fields.Date('Tentative Date Payment')
    payment_type = fields.Selection([
        ('bank', 'Bank'),
        ('cash', 'Cash')
    ], string='Payment Type')
    transaction_number = fields.Char('Transaction Number')
    send_for_approval = fields.Boolean('Send For Approval')
    approved = fields.Boolean('Approved')
    complete = fields.Boolean('Complete')
    payment_date = fields.Date('Payment Date')

    @api.depends('due_date')
    def _get_penalty_days(self):
        if self.due_date:
            if self.due_date < fields.Date.today():
                days_gone = fields.Date.today() - self.due_date
                self.penalty_days = days_gone.days

    @api.depends('penalty_days', 'due_amount')
    def _get_penalty_amount(self):
        if self.penalty_days > 0:
            charge_line = self.partner_id.charges_duration_ids.filtered(
                lambda x: x.start_date <= self.settlement_date and
                          x.end_date >= self.settlement_date and
                          x.charges_type == 'penalty_perday_after_month')
            perday_penalty = self.due_amount * charge_line.charges / 100
            total_penalty = perday_penalty * self.penalty_days
            self.penalty_amount = total_penalty

    @api.depends('due_amount', 'discounted_amount', 'waved_off_amount', 'receive_in_excess', 'penalty_amount')
    def _get_payment_status(self):
        if self.due_amount:
            sum_of_amount = 0.0
            deduct_amount = 0.0
            if self.waved_off_amount or self.receive_in_excess:
                sum_of_amount = self.waved_off_amount + self.receive_in_excess
            if self.discounted_amount:
                sum_of_amount = self.discounted_amount - sum_of_amount
            self.receivable_amount = (self.due_amount + self.penalty_amount) - sum_of_amount
        if self.receivable_amount and self.received_amount:
            if self.receivable_amount - self.received_amount == 0.0:
                self.payment_status = 'paid'
            if self.received_amount < self.receivable_amount:
                self.payment_status = 'partial'

    @api.multi
    def start_followup(self):
        leads = self.env['crm.lead'].search([
            ('user_id', '=', self.env.user.id),
            ('is_running', '=', True),
            ('id', '!=', self.id)
        ])
        if leads:
            raise UserError(_('Kindly close your other followup leads to proceed this one!'))
        folloup_id = self.env['follow.up.details'].create({
            'lead_id': self.id,
            'user_id': self.env.user.id,
            'start_timedate': fields.Datetime.now()
        })
        self.is_running = True
        self.running_followup_id = folloup_id.id

    @api.multi
    def stop_followup(self):
        self.is_running = False
        self.running_followup_id.end_timedate = fields.Datetime.now()
        self.running_followup_id = ''

    @api.multi
    def complete_settlement(self):
        if self.received_amount <= 0:
            raise UserError(_('Amount Should be not 0.0!'))
        stage_id = self.env.ref('capup_extends.stage_complete')
        self.with_context({'write_force': True}).stage_id = stage_id.id
        self.complete = True
        if self.settlement_id:
            self.settlement_id.write({
                'state': 'complete',
                'payment_status': 'paid',
                'received_amount': self.received_amount,
                'discounted_amount': self.discounted_amount,
                'waved_off_amount': self.waved_off_amount,
                'receive_in_excess': self.receive_in_excess,
                'extra_amount': self.extra_amount
            })
        if self.vendor_settlement_id:
            self.vendor_settlement_id.write({
                'state': 'complete',
                'payment_status': 'paid',
                'received_amount': self.received_amount,
                'discounted_amount': self.discounted_amount,
                'waved_off_amount': self.waved_off_amount,
                'receive_in_excess': self.receive_in_excess,
                'extra_amount': self.extra_amount
            })

    @api.multi
    def write(self, vals):
        if vals.get('stage_id') and 'write_force' not in self.env.context:
            raise UserError(_('You cant do this operation!'))
        res = super(CrmLead, self).write(vals)
        return res

    @api.multi
    def send_approval(self):
        stage_id = self.env.ref('capup_extends.stage_send_approval')
        self.with_context({'write_force': True}).stage_id = stage_id.id
        self.send_for_approval = True

    @api.multi
    def approved_crm(self):
        stage_id = self.env.ref('capup_extends.stage_approved')
        self.with_context({'write_force': True}).stage_id = stage_id.id
        self.approved = True


class FollowUpDetails(models.Model):
    _name = 'follow.up.details'

    lead_id = fields.Many2one('crm.lead', string='Lead')
    user_id = fields.Many2one('res.users', string='User')
    start_timedate = fields.Datetime('Start Followup')
    end_timedate = fields.Datetime('End Followup')
    duration = fields.Float('Total Duration')
    desc = fields.Text('Desc')


