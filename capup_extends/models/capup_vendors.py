# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    client_id_ref = fields.Char('Client ID')
    vendor_id_ref = fields.Char('Vendor ID')
    capup_vendors_ids = fields.One2many('capup.vendors', 'capup_vendor_id', 'CapUp Vendors', required=True)
    vendor_status = fields.Selection([
        ('active', 'Active'),
        ('deactive', 'Deactive'),
        ('pending', 'Pending')
    ], string='Vendor Status')
    vendor_approved_by = fields.Many2one('res.users', string='Vendor Approved By')
    vendor_approved_date = fields.Date('Vendor Approved Date')
    payment_term = fields.Selection([
        ('immediate', 'Immediate'),
        ('15_days', '15 Days'),
        ('30_days', '30 Days'),
        ('45_days', '45 Days')
    ], string='Payment Term')

    charge_per_month = fields.Float('Charges per month (30 Days)(%)')
    penalty_perday_after_month = fields.Float('Penalty per Day after 30 Days(%)')
    grn_interest_type = fields.Selection([
        ('percentage', '%'),
        ('amount', 'Amount')
    ], string='GRN Penal Interest Type')
    grn_value = fields.Float('GRN Value')
    grn_term = fields.Selection([
        ('immediate', 'Immediate'),
        ('15_days', '15 Days'),
        ('30_days', '30 Days'),
        ('45_days', '45 Days')
    ], string='GRN Term')
    delivery_interest_type = fields.Selection([
        ('percentage', '%'),
        ('amount', 'Amount')
    ], string='Delivery Note Penal Interest')
    delivery_value = fields.Float('Delivery Value')
    delivery_term = fields.Selection([
        ('immediate', 'Immediate'),
        ('15_days', '15 Days'),
        ('30_days', '30 Days'),
        ('45_days', '45 Days')
    ], string='Delivery Term')
    client_prefix = fields.Char('Client Prefix')
    charges_duration_ids = fields.One2many('charges.duration', 'partner_id', 'Charges Duration')
    agreement_ids = fields.One2many('capup.agreement', 'partner_id', 'Agreement')

    @api.model
    def create(self, vals):
        if vals.get('supplier') and vals.get('client_id'):
            client_id = self.env['res.partner'].browse(vals.get('client_id'))
            vals['vendor_id_ref'] = client_id.client_prefix + self.env['ir.sequence'].next_by_code('res.partner')
            vals['created_by'] = self.env.user.id
        # if vals.get('customer'):

        res = super(ResPartner, self).create(vals)
        return res

    @api.multi
    def get_settlements(self):
        print("ca;=======================")

    @api.multi
    def approve_vendor(self):
        self.vendor_status = 'active'
        self.vendor_approved_by = self.env.user.id
        self.vendor_approved_date = fields.Date.today()


class CapUpVendors(models.Model):
    _name = "capup.vendors"
    _inherits = {'res.partner': 'capup_vendor_id'}

    capup_vendor_id = fields.Many2one(
        'res.partner', 'CapUp Vendor',
        auto_join=True, index=True, ondelete="cascade", required=True)


class ChargesDuration(models.Model):
    _name = 'charges.duration'

    partner_id = fields.Many2one('res.partner', string='Partner')
    charges_type = fields.Selection([
        ('charge_per_month', 'Charges Per Month'),
        ('penalty_perday_after_month', 'Penalty Per Day after month')
    ], string='Charges Type')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    charges = fields.Float('Charges (%)')


class ChargesDuration(models.Model):
    _name = 'capup.agreement'

    partner_id = fields.Many2one('res.partner', string='Partner')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    description = fields.Char('Description')
    attachment_type = fields.Selection([
        ('agreement', 'Agreement'),
        ('invoice', 'Invoice'),
        ('delivery', 'Delivery Note'),
        ('grn', 'GRN'),
        ('other', 'Other')
    ], string='Attachment Type')
    attachments = fields.Binary('Attachment')
