# -*- coding: utf-8 -*-
from odoo import api, models, fields, _


class ScheduledMail(models.Model):
    _name = "scheduled.mail"

    scheduled_type = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Sent')
    ], string='Scheduled Type')
    sent_to = fields.Selection([
        ('vendor', 'Vendor'),
        ('client', 'Client'),
        ('both', 'Both')
    ], string='Sent To')
    client_id = fields.Many2one('res.partner', string='Partner')
    # vendor_id = fields.Many2one('res.partner', string='Vendor')
    email_template_id = fields.Many2one('mail.template', string='Email Template')
    settlement_id = fields.Many2one('client.settlement', string='Settlement ID')
    vendor_settlement_id = fields.Many2one('vendor.settlement', string='Settlement ID')
    schedule_date = fields.Date('Schedule Date')

    def send_mail(self):
        scheduled_mails = self.search([
            ('scheduled_type', '=', 'draft'),
            ('schedule_date', '=', fields.Date.today())
        ])
        for record in scheduled_mails:
            partner_email = record.client_id.email
            record.email_template_id.send_mail(
                record.id, force_send=True, raise_exception=True, email_values={'email_to': partner_email}
            )
            record.scheduled_type = 'sent'




