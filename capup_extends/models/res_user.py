# -*- coding: utf-8 -*-
from odoo import api, models, fields, _
from odoo.exceptions import AccessError, UserError, ValidationError


class ResUsers(models.Model):
    _inherit = "res.users"

    @api.model
    def create(self, vals):
        if vals.get('in_group_66') or vals.get('in_group_67'):
            # self.env.user.has_group('base.group_system'):
            if self.env.user.has_group('partner_extends.group_system_manager') or self.env.user.has_group('partner_extends.group_crm_executive'):
                raise AccessError(_(
                    "You do not have the rights to create user."))
        res = super(ResUsers, self).create(vals)
        return res



