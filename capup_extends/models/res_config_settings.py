# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    first_payment_reminder = fields.Char('Send First payment reminder')
    second_payment_reminder = fields.Char('Send Second payment reminder')
    first_overdue_reminder = fields.Char('Send First OverDue reminder')
    second_overdue_reminder = fields.Char('Send Second OverDue reminder')
    third_overdue_reminder = fields.Char('Send Third OverDue reminder')

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        Params = self.env['ir.config_parameter'].sudo()
        res.update({
            'first_payment_reminder': Params.get_param('first_payment_reminder'),
            'second_payment_reminder': Params.get_param('second_payment_reminder'),
            'first_overdue_reminder': Params.get_param('first_overdue_reminder'),
            'second_overdue_reminder': Params.get_param('second_overdue_reminder'),
            'third_overdue_reminder': Params.get_param('third_overdue_reminder')
        })
        return res

    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].set_param(
            "first_payment_reminder", self.first_payment_reminder or '',
        )
        self.env['ir.config_parameter'].set_param(
            "second_payment_reminder", self.second_payment_reminder or ''
        )
        self.env['ir.config_parameter'].set_param(
            "first_overdue_reminder", self.first_overdue_reminder or '',
        )
        self.env['ir.config_parameter'].set_param(
            "second_overdue_reminder", self.second_overdue_reminder or ''
        )
        self.env['ir.config_parameter'].set_param(
            "third_overdue_reminder", self.third_overdue_reminder or ''
        )
